# JooqSpringBootDemo
Run Project with below commands
1. mvnw clean
2. mvnw install
3. mvnw spring-boot:run

Default Port: 8080
Swagger url: http://localhost:8080/swagger-ui.html

Request for create schema API:

`POST - localhost:8080/db/schemas/db-jooq`

Note: "db-jooq" is schema name

Request for create table API:

`POST - localhost:8080/db/schemas/db-jooq/table`

Request Payload:

`{
     "tableName": "book",
     "fields": [
         {
             "name": "id",
             "dataType": "integer",
             "primaryKey": true
         },
         {
             "name": "book_name",
             "dataType": "varchar",
             "primaryKey": false
         }
     ]
 } `
