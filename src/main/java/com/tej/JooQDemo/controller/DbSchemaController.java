package com.tej.JooQDemo.controller;

import com.tej.JooQDemo.model.TableStructureModel;
import com.tej.JooQDemo.service.DbSchemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.sql.SQLException;

@RestController
@RequestMapping("/db")
public class DbSchemaController {

    private final DbSchemaService dbSchemaService;

    @Autowired
    public DbSchemaController(DbSchemaService dbSchemaService) {
        this.dbSchemaService = dbSchemaService;
    }

    @PostMapping("/schemas/{schemaId}/table")
    public void createTable(@PathVariable String schemaId, @RequestBody TableStructureModel request) throws SQLException {
        DataSource dataSource = this.dbSchemaService.createTable(schemaId, request);
        dataSource.getConnection().close();
    }

    @PostMapping("/schemas/{schemaId}")
    public void createSchema(@PathVariable String schemaId) throws SQLException {
        DataSource dataSource = this.dbSchemaService.createSchema(schemaId);
        dataSource.getConnection().close();
    }
}
