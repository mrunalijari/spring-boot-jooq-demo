package com.tej.JooQDemo.model;

import java.util.ArrayList;
import java.util.List;

public class TableStructureModel {

    private String tableName;
    private List<FieldStructureModel> fields = new ArrayList<>();

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<FieldStructureModel> getFields() {
        return fields;
    }

    public void setFields(List<FieldStructureModel> fields) {
        this.fields = fields;
    }
}
