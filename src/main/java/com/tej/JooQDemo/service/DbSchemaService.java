package com.tej.JooQDemo.service;

import com.tej.JooQDemo.model.TableStructureModel;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;

import java.sql.SQLException;

import static org.jooq.impl.DSL.primaryKey;
import static org.jooq.impl.SQLDataType.INTEGER;
import static org.jooq.impl.SQLDataType.VARCHAR;

@Service
public class DbSchemaService {

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;

    @Transactional
    public DataSource createSchema(String schemaId) {
        DataSource dataSource = getDataSource("");
        Configuration config = getConfig(dataSource);
        DSL.using(config).createSchemaIfNotExists(schemaId).execute();

        return dataSource;
    }

    @Transactional
    public DataSource createTable(String schemaId, TableStructureModel request) throws SQLException {

        createSchema(schemaId).getConnection().close();

        DataSource dataSource = getDataSource(schemaId);
        Configuration config = getConfig(dataSource);

        CreateTableColumnStep table = DSL.using(config).createTableIfNotExists(request.getTableName());

        request.getFields().forEach(field -> {
            if (field.getDataType().equals("integer")) {
                table.column(field.getName(), INTEGER);
            } else if (field.getDataType().equals("varchar")) {
                table.column(field.getName(), VARCHAR);
            }

            if (field.isPrimaryKey()) {
                table.constraints(primaryKey(field.getName()));
            }
        });

        table.execute();

        return dataSource;
    }

    private DataSource getDataSource(String schema) {
        HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setJdbcUrl("jdbc:mysql://localhost:3306/" + schema + "?serverTimezone=UTC");
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);
        hikariConfig.setDriverClassName(driverClassName);
        return new HikariDataSource(hikariConfig);
    }

    private Configuration getConfig(DataSource dataSource) {
        Configuration config = new DefaultConfiguration();
        ConnectionProvider connectionProvider = new DataSourceConnectionProvider(dataSource);
        config.set(connectionProvider);
        config.set(SQLDialect.MARIADB);

        return config;
    }
}
